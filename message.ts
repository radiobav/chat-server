export type Message = {
    uuid: string;
    username: string;
    text: string;
    datetime: Date;
  }

class MessageService {
    lastMessages: Message[] = [
        {uuid: crypto.randomUUID(), username: "admin", text: "Chat server started", datetime: new Date()}
    ]

    public addMessage(msg: Message) {
        this.lastMessages = [...this.lastMessages, msg]
    }

    public deleteMessage(uuid: string) {
        this.lastMessages = this.lastMessages.filter(el => el.uuid != uuid)
    }


}

export const messageService = new MessageService()