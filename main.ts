import { Application, Cookies, RouteContext, Router, Status } from "https://deno.land/x/oak/mod.ts";
import { oakCors } from "https://deno.land/x/cors/mod.ts";
import logger from "https://deno.land/x/oak_logger/mod.ts";
import { RateLimiter } from "https://deno.land/x/oak_rate_limit/mod.ts";

import { messageService, Message } from "./message.ts";
import { SessionService, Session } from "./session.ts";

const connectedClients = new Map<string, WebSocket>();
const router = new Router();
const kv = await Deno.openKv("db")

const sessionService = new SessionService(kv, "ES_7ea2081093c3430ca3f8417cc590cb9e", "22cc9e82-41cf-46cf-8ed8-5b527d060ed7")

// Send a message to all connected clients
function broadcastMessage(message: Message) {
  messageService.addMessage(message)
  for (const client of connectedClients.values()) {
    client.send(JSON.stringify({event: "new-message", data: message}));
  }
}

// Delte a message from all connected clients
function broadcastDelete(uuid: string) {
  messageService.deleteMessage(uuid)
  for (const client of connectedClients.values()) {
    client.send(JSON.stringify({event: "delete-message", data: uuid}));
  }
}


async function getSessionFromCookie(cookies: Cookies): Promise<Session> {
  console.debug(cookies)
    if(!cookies.has("bb_chat_session")) {
      throw Error("No session cookie set")
    }
  
    const sessionKey = await cookies.get("bb_chat_session");
    if(!sessionKey ||  sessionKey.trim() === "") {
      throw Error("No session key in cookie")
    }
  
    return await sessionService.getSession(sessionKey)
    
}

// Register a client for the chat
router.post("/chat/session", async (ctx) => {

  try {
    const {username, captchaResponse} = await ctx.request.body.json()

    if(username.trim() === "") {
      ctx.response.status = Status.BadRequest
    }
    const sessionKey = await sessionService.createSession(username, captchaResponse, false)
    const session = await sessionService.getSession(sessionKey);

    const maxAge =  86400 * 90; //86400s = 1 Day
    const cookieString = `bb_chat_session=${sessionKey};Max-Age=${maxAge};SameSite=None;Secure;HttpOnly`

     await ctx.cookies.set("bb_chat_session", sessionKey, {
        sameSite: "None",
        maxAge: maxAge
    });
    
    ctx.response.headers.append("Set-Cookie", cookieString );
    ctx.response.headers.set("Content-Type", "application/json") ;
    ctx.response.status = 200;
    ctx.response.body = session;
  } catch(e) {
    console.error(e)
    ctx.response.status = Status.BadRequest
  }

});

// Returns the session of a client if available
router.get("/chat/session", async (ctx) => {
  try {
    const session = await getSessionFromCookie(ctx.cookies)
    session.banned = false; // Never let the user know he was banned!
    ctx.response.headers.set("Content-Type", "application/json") ;
    ctx.response.status = 200;
    ctx.response.body = session;
  } catch(e) {
    console.error(e.message)
    ctx.response.status = Status.Forbidden
    return;
  }
});

router.post("/chat/message", async (ctx) => {
  try {

    // Will fail if the user is not logged in
    const session = await getSessionFromCookie(ctx.cookies)

    if(session.banned) {
      return; // Fail silently when the user is banned
    }

    const {text} = await ctx.request.body.json()
    const message: Message = {
      uuid: crypto.randomUUID(),
      username: session.username,
      text: text,
      datetime: new Date()
    }
    broadcastMessage(message)
    
  } catch(e) {
    console.error(e)
    ctx.response.status = Status.Forbidden
    return;
  }
});


// Connect a client to the chat socket
router.get("/chat/socket", async (ctx) => {

   const socket = await ctx.upgrade();
   const socketUUID = crypto.randomUUID()

    connectedClients.set(socketUUID, socket);
    console.log(`New client connected: ${socketUUID}`);

    const welcomeMessage: Message = {
      uuid: crypto.randomUUID(),
      username: "Bavariabeats",
      "text": "Willkommen im Chat!",
      datetime: new Date()
    }

    // Send the last 10 message when a client connects
    socket.onopen = () => {
      socket.send(JSON.stringify({event: "new-message", data: welcomeMessage}))
      const lastMessages = messageService.lastMessages
      for (const message of lastMessages) {
        socket.send(JSON.stringify({event: "new-message", data: message}))
      }
    };

    // when a client disconnects, remove them from the connected clients list
    // and broadcast the active users list
    socket.onclose = () => {
      console.log(`Client ${socketUUID} disconnected`);
      connectedClients.delete(socketUUID);
    };

    // We do not expect to retrieve messages from the websocket so do nothing
    socket.onmessage = () => {}

});

router.post("/chat/admin/delete", async (ctx) => {
  try{
    const session = await getSessionFromCookie(ctx.cookies);
    if(!session.admin_access) {
      throw Error("Not admin")
    }

    const {uuid} = await ctx.request.body.json();
    broadcastDelete(uuid);
    
    ctx.response.status = Status.NoContent;
    return;

  } catch (e) {
    console.error(e)
    ctx.response.status = Status.Forbidden
    return;
  }
});

router.post("/chat/admin/bann", async (ctx) => {
  try{
    const session = await getSessionFromCookie(ctx.cookies);
    if(!session.admin_access) {
      throw Error("Not admin")
    }

    const {sessionKey} = await ctx.request.body.json();


  } catch (e) {
    console.error(e)
    ctx.response.status = Status.Forbidden
    return;
  }
});

const app = new Application();
const port = 8080;

const rateLimit = RateLimiter({
  windowMs: 1000, // Window for the requests that can be made in miliseconds.
  max: 3, // Max requests within the predefined window.
  headers: true, // Default true, it will add the headers X-RateLimit-Limit, X-RateLimit-Remaining.
  message: "Too many requests, please try again later.", // Default message if rate limit reached.
  statusCode: 429, // Default status code if rate limit reached.
});

app.use(await rateLimit);
app.use(logger.logger)
app.use(logger.responseTime)
app.use(oakCors({origin: true, credentials:true}));
app.use(router.routes());
app.use(router.allowedMethods());
app.use(async (context) => {
  await context.send({
    root: `/app`,
    index: "index.html",
  });
});

console.log("Listening at http://localhost:" + port);
await app.listen({ port });
