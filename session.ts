export type Session = {
    username: string;
    created: Date;
    banned: boolean; //if true, this session was banned message should not be forwarded to other users
    admin_access: boolean;
  }


export class SessionService {

    kv: Deno.Kv
    hCaptchaSecretKey
    hCaptchaSiteKey

    constructor(kv: Deno.Kv, hCaptchaSecretKey: string, hCaptchaSiteKey: string) {
        this.kv = kv
        this.hCaptchaSecretKey = hCaptchaSecretKey
        this.hCaptchaSiteKey = hCaptchaSiteKey
    }

    public async createSession(username: string, captchaResponse: string, is_admin: boolean): Promise<string> {
        if(!await this.verifyCaptcha(captchaResponse)) {
            throw Error("Invalid captcha")
        }
        
        const sessionKey = crypto.randomUUID()

        const session: Session = {username: username, created: new Date(), banned: false, admin_access: is_admin}
        const res = await this.kv.set(["session", sessionKey], session, {expireIn: 86400 * 90 * 1000 }) //86400s = 1 Day

        if(res.ok) {
            return sessionKey
        } else {
            throw Error("Unable to create Session in Deno.Kv")
        }

    }

    public async getSession(sessionSecret: string): Promise<Session> {

        const result = await this.kv.get<Session>(["session", sessionSecret])
        if(result.value) {
            const session = result.value

            // Endless sessions for now onöy removed by kv db
            //if(session.valid_until <= Temporal.Now.zonedDateTimeISO()) {
            //    throw Error(`Session ${sessionSecret} is no longer valid!`)
            //} 

            return session;
        }
        throw Error(`Session ${sessionSecret} not found!`)
    }

    public async bannSession(sessionSecret: string): Promise<void> {
        const session = (await this.kv.get<Session>(["session", sessionSecret])).value

        if(!session) {
            throw Error("Session not found")
        }

        session.banned = true
        await this.kv.set(["session", sessionSecret], session, {expireIn: 86400 * 90 * 1000 }) //86400s = 1 Day
    }

    private async verifyCaptcha(capthchaResponse: string): Promise<boolean> {
        try {

            const res = await fetch('https://api.hcaptcha.com/siteverify', {
                method: 'POST',
                headers:{
                'Content-Type': 'application/x-www-form-urlencoded'
                },    
                body: new URLSearchParams({
                    'secret': this.hCaptchaSecretKey,
                    'sitekey': this.hCaptchaSiteKey,
                    'response': capthchaResponse
                }).toString()
            });
            const data = await res.json()
            return data.success
        } catch(e) {
            console.error(e)
            return false
        }
    }
    

}
